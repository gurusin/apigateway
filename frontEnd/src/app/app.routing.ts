import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {ApiPageComponent} from "./api-page/api-page.component";



const appRoutes: Routes = [

  { path: 'login', component: LoginComponent },
  { path: 'api', component: ApiPageComponent },
  // otherwise redirect to login
  { path: '**', redirectTo: 'login' }
];

export const routing = RouterModule.forRoot(appRoutes);
