import { Component, OnInit } from '@angular/core';
import {AlertService} from "../service/alert.service";
import {APIServiceService} from "../service/apiservice.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-api-page',
  templateUrl: './api-page.component.html',
  styleUrls: ['./api-page.component.css']
})
export class ApiPageComponent implements OnInit {

  processing = true;
  users=[];


  constructor(private alertService:AlertService, private apiService:APIServiceService,
              private router: Router) {
    this.apiService.getUsers().subscribe(
      data =>{
          this.processing = false;
        this.alertService.success("Successully connected to the User API");
          this.users = data;
      },
      error =>{
        this.alertService.error(error.error.message,true);
        this.router.navigate(['login']);
      }
    );
  }

  ngOnInit() {

  }

}
