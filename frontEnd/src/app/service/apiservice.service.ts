import {Injectable} from '@angular/core';
import {Headers} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class APIServiceService {

  constructor(private http: HttpClient, private router: Router) {
  }

  getUsers(): Observable<any[]> {
    var url = localStorage.getItem("rootUrl") + "remote/userList";
    return this.http.get(url, {headers: this.getHeaders()});
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred: ', error); // for demo only
    return Promise.reject(error.message || error);

  }

  private getHeaders(): HttpHeaders {
    return new HttpHeaders({
      'token': localStorage.getItem("authToken")
    });

  }

}
