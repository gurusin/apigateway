import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'


@Injectable()
export class LoginServiceService {

  constructor(private http:HttpClient) { }

  login(username: string, password: string) {
    return this.http.post<any>('http://localhost:8080/api/login', { username: username, password: password })
      .map(token => {
        // login successful if there's a jwt token in the response
        if (token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('authToken', token);
        }
        return token;
      });
  }
}
