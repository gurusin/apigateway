import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {LoginServiceService} from "../service/login-service.service";
import {AlertService} from "../service/alert.service";
import {APIServiceService} from "../service/apiservice.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router, private loginService:LoginServiceService,
    private alertService:AlertService,
    private apiService:APIServiceService) { }

  ngOnInit() {
  }

  login() {
    this.loading = true;
    this.alertService.success("");
    this.loginService.login(this.model.username, this.model.password)
      .subscribe(
        data => {
          this.router.navigate(["/api"]);
          this.loading = false;
        },
        error => {
           this.alertService.error(error.error.message);
           this.loading = false;
        });
  }

}
