import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing }        from './app.routing';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {LoginServiceService} from "./service/login-service.service";
import {AlertService} from "./service/alert.service";
import { AlertComponent } from './alert/alert.component';
import { ApiPageComponent } from './api-page/api-page.component';
import {APIServiceService} from "./service/apiservice.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AlertComponent,
    ApiPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing
  ],
  providers: [LoginServiceService,AlertService,APIServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
