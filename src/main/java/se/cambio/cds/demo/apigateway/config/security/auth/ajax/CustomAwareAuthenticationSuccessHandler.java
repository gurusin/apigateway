package se.cambio.cds.demo.apigateway.config.security.auth.ajax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import se.cambio.cds.demo.apigateway.token.TokenHandler;
import se.cambio.cds.demo.apigateway.token.TokenNotFoundException;


@Component
public class CustomAwareAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final ObjectMapper mapper;

    @Autowired
    private TokenHandler tokenHandler;

    @Autowired
    public CustomAwareAuthenticationSuccessHandler(final ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    private HttpSession httpSession;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {

        String username = (String) httpSession.getAttribute("username");
        String password = (String) httpSession.getAttribute("password");

        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        try {
            mapper.writeValue(response.getWriter(), tokenHandler.retrieveToken(username, password));

        }catch(TokenNotFoundException te)
        {
            throw new ServletException("Unable to retrive a token. Please check username and password ");

        } catch (Throwable e) {
            String message = e.getMessage();
            if (message== null)
            {
                message = e.getClass().getName() + " has Occured. Please check the server log files";
                // To check the strange exception !!!
                e.printStackTrace();
            }
            throw new ServletException(message);
        }finally {
            clearAuthenticationAttributes(request);
        }

    }

    /**
     * Removes temporary authentication-related data which may have been stored
     * in the session during the authentication process..
     * 
     */
    protected final void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);

        if (session == null) {
            return;
        }

        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
}
