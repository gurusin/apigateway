package se.cambio.cds.demo.apigateway.token;

import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

/**
 * Provide methods to invoke the token service with retries
 * @author  : Sudarshana Gurusinghe
 * @Since : Jan 2018
 */

@Service
public interface TokenService {


    public String retrieveToken(final String userName, final String password) throws TokenNotFoundException,Throwable;

    public TokenStatus isValidToken(final String token) throws AuthenticationException,TokenNotFoundException;
}
