package se.cambio.cds.demo.apigateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import se.cambio.cds.demo.apigateway.token.TokenHandler;


@RestController
public class MainController {

    @Autowired
    private TokenHandler tokenHandler;

    @RequestMapping(value="/api/login", method=RequestMethod.POST)
    public void login() {

    }
}
