package se.cambio.cds.demo.apigateway.config.security.auth;


import org.springframework.security.authentication.AbstractAuthenticationToken;
import se.cambio.cds.demo.apigateway.config.security.model.token.RawAccessJwtToken;
import se.cambio.cds.demo.apigateway.token.TokenStatus;

/**
 * An {@link org.springframework.security.core.Authentication} implementation
 * to handle the CDS tokens
 * 
 * @author Sudarshana Gurusinghe
 * @Since Jan 2018
 *
 *
 */
public class CDSAuthenticationToken extends AbstractAuthenticationToken {

    private String token;
    private TokenStatus tokenStatus;

    public CDSAuthenticationToken(String token) {
        super(null);
        this.token = token;
        this.setAuthenticated(false);
    }

    public CDSAuthenticationToken( final TokenStatus tokenStatus, String token) {
        super(null);
        this.eraseCredentials();
        this.token = token;
        this.tokenStatus = tokenStatus;
        super.setAuthenticated(true);
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        if (authenticated) {
            throw new IllegalArgumentException(
                    "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }
        super.setAuthenticated(false);
    }

    public String getToken() {
        return token;
    }

    @Override
    public Object getPrincipal() {
        return tokenStatus;
    }

    @Override
    public void eraseCredentials() {        
        super.eraseCredentials();
        this.token = null;
        this.tokenStatus = null;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
