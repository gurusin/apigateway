package se.cambio.cds.demo.apigateway.config.security.exceptions;

import org.springframework.security.core.AuthenticationException;
import se.cambio.cds.demo.apigateway.config.security.model.token.RawAccessJwtToken;

/**
 * Exception class to indicate an expired token
 */

public class JwtExpiredTokenException extends AuthenticationException {

    private RawAccessJwtToken token;

    public JwtExpiredTokenException(RawAccessJwtToken token, String msg, Throwable t) {
        super(msg, t);
        this.token = token;
    }

    public String token() {
        return this.token.getToken();
    }
}
