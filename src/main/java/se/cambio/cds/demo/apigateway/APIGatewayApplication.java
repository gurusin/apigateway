package se.cambio.cds.demo.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.retry.annotation.EnableRetry;


/**
 * StartUp application for  the API Gateway
 * @author : Sudarshana Gursinghe
 * @Since : Jan 2018
 */
@SpringBootApplication
@EnableConfigurationProperties
@EnableZuulProxy
@EnableRetry
public class APIGatewayApplication {
	public static void main(String[] args) {

		SpringApplication.run(APIGatewayApplication.class, args);
	}
}
