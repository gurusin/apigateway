package se.cambio.cds.demo.apigateway.config.security.model.token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;

import se.cambio.cds.demo.apigateway.config.security.exceptions.JwtExpiredTokenException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import se.cambio.cds.demo.apigateway.token.TokenStatus;

/**
 * Place holder to store token
 *
 */
public class RawAccessJwtToken {

    private String token;
    private TokenStatus tokenStatus;
    
    public RawAccessJwtToken(String token, TokenStatus tokenStatus) {
        this.tokenStatus = tokenStatus;
        this.token = token;
    }


    public String getToken() {
        return token;
    }
}
