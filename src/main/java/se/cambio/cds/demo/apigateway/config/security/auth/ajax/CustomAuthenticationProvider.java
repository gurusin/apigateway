package se.cambio.cds.demo.apigateway.config.security.auth.ajax;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.servlet.http.HttpSession;

/**
 *  Authentication Provider to verify username and password
 *  @author : Sudarshana Gurusinghe
 *  @since : Jan 2018
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private HttpSession httpSession;

    @Autowired
    public CustomAuthenticationProvider() {

    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.notNull(authentication, "No authentication data provided");

        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        if (username == null || password == null)
        {
            throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
        }
        httpSession.setAttribute("username", username);
        httpSession.setAttribute("password", password);
        return new UsernamePasswordAuthenticationToken(username,password,null);

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
