package se.cambio.cds.demo.apigateway.token;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.regexp.internal.RE;
import jdk.nashorn.internal.parser.JSONParser;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


import org.springframework.security.core.AuthenticationException;
import sun.security.validator.ValidatorException;

import javax.net.ssl.SSLException;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles the methods of token creation and token handler
 *
 * @Author : Sudarshana Gurusinghe
 */

@Component
public class TokenHandler implements TokenService {
    private static final String TOKEN_REQUEST_URL = "https://idp.cambiocds.com/auth/realms/cds-platform/protocol/openid-connect/token";
    private static final String TOKEN_VALIDATE_URL = "https://idp.cambiocds.com/auth/realms/cds-platform/protocol/openid-connect/token/introspect";

    private static final int HTTP_OK = 200;

    @Override
    @Retryable(value ={TokenNotFoundException.class} , maxAttempts = 3, backoff = @Backoff(delay = 3000))
    public String retrieveToken(final String userName, final String password) throws TokenNotFoundException, Throwable {
        HttpResponse response = null;
        try {

            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(TOKEN_REQUEST_URL);
            List<NameValuePair> arguments = getNameValuePairs(userName,password);
            post.setEntity(new UrlEncodedFormEntity(arguments));
            response = client.execute(post);
            // Exit condition
            if (response.getStatusLine().getStatusCode() == HTTP_OK) {

                final String token = EntityUtils.toString(response.getEntity());
                return parseToken(token);

            } else {

                throw new TokenNotFoundException("Retry");
            }
        } catch (IOException e) {

            throw new TokenNotFoundException("Error Occured : Retrying :" + e.getMessage());

        } catch (Throwable ve) {
            // To handle any unknown exception
            throw ve;
        }
    }


    @Override
    @Retryable(value ={TokenNotFoundException.class},maxAttempts = 3, backoff = @Backoff(delay = 5000))
    public TokenStatus isValidToken(final String token) throws TokenNotFoundException,AuthenticationException {
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(TOKEN_VALIDATE_URL);
            List<NameValuePair> arguments = getParametersForTokenValidation(token);
            post.setEntity(new UrlEncodedFormEntity(arguments));
            HttpResponse response = client.execute(post);
            final TokenValidateResponse result = parseResponse(EntityUtils.toString(response.getEntity()));

            TokenStatus status = TokenStatus.VALID;
            if (result.getExp() == 0) {
                status = TokenStatus.INVALID;
            } else if (!result.isActive()) {
                status = TokenStatus.EXPIRED;
            }
            return status;
        } catch (IOException e) {
            throw new TokenNotFoundException("Error Occured : Retrying :" + e.getMessage());
        } catch (Exception e) {
            throw new BadCredentialsException(e.getMessage());
        }
    }

    private List<NameValuePair> getNameValuePairs(final String username, final String password) {
        List<NameValuePair> arguments = new ArrayList<>();
        arguments.add(new BasicNameValuePair("grant_type", "password"));
        arguments.add(new BasicNameValuePair("client_id", "gateway"));
        arguments.add(new BasicNameValuePair("username", username));
        arguments.add(new BasicNameValuePair("password", password));
        arguments.add(new BasicNameValuePair("client_secret", "73a67ef3-677e-4ef4-931e-5cfd45daee01"));
        return arguments;
    }

    private List<NameValuePair> getParametersForTokenValidation(final String token) {
        List<NameValuePair> arguments = new ArrayList<>();
        arguments.add(new BasicNameValuePair("token", token));
        arguments.add(new BasicNameValuePair("client_id", "gateway"));
        arguments.add(new BasicNameValuePair("client_secret", "73a67ef3-677e-4ef4-931e-5cfd45daee01"));
        return arguments;
    }


    private String parseToken(final String token) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        AuthToken obj = mapper.readValue(token, AuthToken.class);
        return obj.getAccess_token();
    }

    private TokenValidateResponse parseResponse(final String result) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(result, TokenValidateResponse.class);
    }
}
