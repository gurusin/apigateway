package se.cambio.cds.demo.apigateway.config.security.exceptions;

import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * Exception class to indicate a non supported authentication method
 */
public class AuthMethodNotSupportedException extends AuthenticationServiceException {

    public AuthMethodNotSupportedException(String msg) {
        super(msg);
    }
}
