package se.cambio.cds.demo.apigateway.token;

/**
 * Defines the status of the token after verification
 * @author :Sudarshana Gurusinghe
 * @since : Jan 2018
 */
public enum TokenStatus {

    VALID,INVALID,EXPIRED;
}
