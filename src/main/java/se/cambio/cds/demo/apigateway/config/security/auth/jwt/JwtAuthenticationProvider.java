package se.cambio.cds.demo.apigateway.config.security.auth.jwt;

import com.netflix.zuul.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import se.cambio.cds.demo.apigateway.config.security.auth.CDSAuthenticationToken;
import se.cambio.cds.demo.apigateway.config.security.model.token.RawAccessJwtToken;

import se.cambio.cds.demo.apigateway.token.TokenHandler;
import se.cambio.cds.demo.apigateway.token.TokenNotFoundException;
import se.cambio.cds.demo.apigateway.token.TokenService;
import se.cambio.cds.demo.apigateway.token.TokenStatus;

import javax.servlet.http.HttpSession;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider
{

    @Autowired
    private TokenService tokenHandler;

    @Autowired
    private HttpSession httpSession;
    
    @Autowired
    public JwtAuthenticationProvider() {

    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        CDSAuthenticationToken rawAccessToken = (CDSAuthenticationToken) authentication;
        String token = rawAccessToken.getToken();
        try {
            TokenStatus status = tokenHandler.isValidToken(token);
            if (status == TokenStatus.EXPIRED) {
                token = getToken();
                status = TokenStatus.VALID;

            } else if (status == TokenStatus.INVALID) {
                throw new AuthenticationServiceException("Invalid Token ");
            }
            RequestContext.getCurrentContext().addZuulRequestHeader("token", token);
            return new CDSAuthenticationToken(status, token);
        }catch(TokenNotFoundException ne)
        {
            System.out.println("Retry Exception ");
        } catch (Throwable e) {
            throw new AuthenticationServiceException("Error in validating token");
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (CDSAuthenticationToken.class.isAssignableFrom(authentication));
    }

    private String  getToken() throws Throwable
    {
        final String username =(String)httpSession.getAttribute("username");
        final String password =(String)httpSession.getAttribute("password");
        return tokenHandler.retrieveToken(username,password);
    }
}
