package se.cambio.cds.demo.apigateway.token;

/**
 * This is the Exception class that would be thrown in the event of a token
 * Server is slow.
 */
public class TokenNotFoundException  extends Exception
{
    public TokenNotFoundException(String message) {
        super(message);
    }
}
